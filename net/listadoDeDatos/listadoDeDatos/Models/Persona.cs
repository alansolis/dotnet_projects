﻿using System;

namespace listadoDeDatos.Models
{
	public class Persona
	{
		public int codigo { get; set;}
		public String nombre { get; set;}
		public String apPaterno { get; set;}
		public String apMaterno { get; set;}

		public Persona ()
		{
			
		}
	}
}

