﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using listadoDeDatos.Models;

namespace listadoDeDatos.Controllers
{
	public class HomeController : Controller
	{
		public List<Persona> lista;
		public Persona persona01;
		public Persona persona02;
		public Persona persona03;
		public Persona persona04;
		public Persona persona05;

		public HomeController()
		{
			this.lista 		= new List<Persona>();
			this.persona01 	= new Persona();
			this.persona02 	= new Persona();
			this.persona03 	= new Persona();
			this.persona04 	= new Persona();
			this.persona05 	= new Persona();

			this.persona01.codigo 		= 1;
			this.persona01.nombre 		= "Alan";
			this.persona01.apPaterno 	= "Solis";
			this.persona01.apMaterno	= "Flores";

			this.persona02.codigo 		= 2;
			this.persona02.nombre 		= "Alan";
			this.persona02.apPaterno 	= "Solis";
			this.persona02.apMaterno	= "Flores";

			this.persona03.codigo 		= 3;
			this.persona03.nombre 		= "Alan";
			this.persona03.apPaterno 	= "Solis";
			this.persona03.apMaterno	= "Flores";

			this.persona04.codigo 		= 4;
			this.persona04.nombre 		= "Alan";
			this.persona04.apPaterno 	= "Solis";
			this.persona04.apMaterno	= "Flores";

			this.persona05.codigo 		= 5;
			this.persona05.nombre 		= "Alan";
			this.persona05.apPaterno 	= "Solis";
			this.persona05.apMaterno	= "Flores";

			this.lista.Add(persona01);
			this.lista.Add(persona02);
			this.lista.Add(persona03);
			this.lista.Add(persona04);
			this.lista.Add(persona05);
		}

		public ActionResult Index()
		{
			return View(this.lista.ToList());
		}
	}
}

